<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    public function all(): array
    {
        return $this->cars;
    }

    public function run(): Car
    {
        $carWinner = [];
        $raceLength = $this->getLapLength() * $this->getLapsNumber();
        foreach ($this->all() as $k => $car) {
            $pitStopTime = ($raceLength * $car->getFuelConsumption() * $car->getPitStopTime())/($car->getFuelTankVolume() * 100); 
            $raceTime = ($raceLength * 3600 / $car->getSpeed()) + $pitStopTime;
            $carWinner[$car->getId()] = $raceTime;
        }
        asort($carWinner);
        $winnerId = (int) array_key_first($carWinner);
        return $winnerId;
    }
}